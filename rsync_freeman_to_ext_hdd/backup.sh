#!/bin/sh

BACKUP_LOC=/media/hilga_/freeman/

DIRECTORIES=\
"\
/home/rsharm14/research/ \
/home/rsharm14/svn/ \
/home/rsharm14/repository/ \
/home/rsharm14/build/ \
/home/rsharm14/rescue/ \
/home/rsharm14/Desktop/ \
"

# Backup process
for dir in $DIRECTORIES
do
    basename=`basename $dir`
    rsync -arv rsharm14@rsass-homer.uncc.edu:$dir $BACKUP_LOC/$basename
done

